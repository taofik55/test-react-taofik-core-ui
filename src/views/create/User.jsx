import React, { useState, useEffect } from 'react'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import axios from 'axios';

export default function User() {
    const [name, setName] = useState("")
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const [errorName, setErrorName] = useState(false)
    const [errorPassword, setErrorPassword] = useState(false)
    const [errorUsername, setErrorUsername] = useState(false)

    const handleName = (e) => {
        const val = e.target.value;
        setName(val);
        if (val.length <= 3) {
            setErrorName(true);
        } else {
            setErrorName(false);
        }
    };
    const handleUsername = (e) => {
        const val = e.target.value;
        setUsername(val);
        if (val.length <= 3) {
            setErrorUsername(true);
        } else {
            setErrorUsername(false);
        }
    };
    const handlePassword = (e) => {
        const val = e.target.value;
        setPassword(val);
        if (val.length <= 8) {
            setErrorPassword(true);
        } else {
            setErrorPassword(false);
        }
    };
    const create = () => {
        if (!errorName && !errorUsername && !errorPassword) {
            // submit ke database here
            const urlPost = 'https://sharingvision-backend.herokuapp.com/user'
            const data = {
                name: name,
                username: username,
                password: password,
            }
            axios.post(urlPost, data)
                .then(response => {
                    console.log(response); //Kena CORS untuk melakukan POST data
                })
        } else {
            // ada error
            console.log("masih ada error gan");
        }
    };

    return (
        <div>
            <h1>Create User</h1>
            <hr />
            <TextField
                label="Name"
                variant="outlined"
                onChange={handleName}
                error={errorName}
            />
            {errorName ? (
                <p>Nama harus lebih dari 3 karakter</p>
            ) : (
                <div></div>
            )}
            <br />
            <TextField
                label="Username"
                variant="outlined"
                onChange={handleUsername}
                error={errorUsername}
            />
            {errorUsername ? (
                <p>Username harus lebih dari 3 karakter</p>
            ) : (
                <div></div>
            )}
            <br />
            <TextField
                label="Password"
                type="password"
                variant="outlined"
                onChange={handlePassword}
                error={errorPassword}
            />
            {errorPassword ? (
                <p>Password harus lebih dari 7 karakter</p>
            ) : (
                <div></div>
            )}
            <br />

            {(errorName || errorUsername || errorPassword) ? (
                <Button variant="outlined" color="primary" disabled>
                    Can't Create
                </Button>
            ) : (
                <Button variant="outlined" color="primary" onClick={create}>
                    Submit
                </Button>
            )}
        </div>
    )
}
