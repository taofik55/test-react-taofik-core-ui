import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Pagination from "@material-ui/lab/Pagination";
import "./Dashboard.css"
import CircularProgress from '@material-ui/core/CircularProgress';

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});

export default function Dashboard() {
  const classes = useStyles();
  const [data, setData] = useState([]);
  const [lim, setLim] = useState(5);
  const [off, setOff] = useState(1);
  const [totalPage, setTotalPage] = useState(1)
  const [dataFetched, setDataFetched] = useState(false)

  const getDatas = async (limit, offset) => {
    try {
      setDataFetched(false)
      const res = await axios.get(`https://sharingvision-backend.herokuapp.com/user/` + limit + `/` + offset);
      if (res.status === 200) {
        setData(res.data.data);
        setDataFetched(true)
        //console.log(res.data)
        setTotalPage(Math.ceil(res.data.total / lim))
      }

    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    getDatas(lim, off);

  }, [])

  function changePage(page) {
    getDatas(lim, page);
  }


  return (
    <div>
      <h1>Table User</h1>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="customized table">
          <TableHead>
            <TableRow>
              <StyledTableCell>Id</StyledTableCell>
              <StyledTableCell>Username</StyledTableCell>
              <StyledTableCell>Password</StyledTableCell>
              <StyledTableCell>Name</StyledTableCell>
            </TableRow>
          </TableHead>
          {!dataFetched ? <CircularProgress /> : <TableBody>
            {data.map((item) => {
              return (
                <StyledTableRow key={item.id}>
                  <StyledTableCell component="th" scope="row">
                    {item.id}
                  </StyledTableCell>
                  <StyledTableCell>
                    {item.username}
                  </StyledTableCell>
                  <StyledTableCell>
                    {item.password}
                  </StyledTableCell>
                  <StyledTableCell>
                    {item.name}
                  </StyledTableCell>
                </StyledTableRow>
              );
            })}
          </TableBody>}

        </Table>
      </TableContainer>
      <Pagination count={totalPage} onChange={(event, val) => changePage(val)} size='large' />
    </div>
  )
}
