import React, { useState, useEffect } from 'react'
import {
    CButton,
    CModal,
    CModalBody,
    CModalFooter,
    CModalHeader,
    CModalTitle,
} from '@coreui/react'
import TextField from '@material-ui/core/TextField';
import axios from 'axios';
import CircularProgress from '@material-ui/core/CircularProgress';

export default function User() {
    const [createPop, setCreatePop] = useState(false)
    const [isDeleted, setIsDeleted] = useState(false)
    const [userId, setUserId] = useState()
    const [userIds, setUserIds] = useState([])
    const [data, setData] = useState([]);
    const [dataFetched, setDataFetched] = useState(false)
    const [name, setName] = useState("")
    const [username, setUsername] = useState("")
    var arr = new Array();

    function deleteData() {
        axios.delete('https://sharingvision-backend.herokuapp.com/user/' + userId).
            then(response => {
                console.log(response); //Kena CORS untuk melakukan POST data
                if (response.status === 200) {
                    setCreatePop(false);
                    setIsDeleted(true);
                }
            })
    }

    const handleId = (e) => {
        const val = e.target.value;
        setUserId(val);

        let i = 0;
        for (let index = 0; index < data.length; index++) {
            if (data[index].id === parseInt(val)) {
                setName(data[index].name);
                setUsername(data[index].username);
                break;
            }
            
        }
    };

    const handleModal = () => {
        setCreatePop(true)
        getDatas();
    }

    const getDatas = async () => {
        try {
            setDataFetched(false)
            const res = await axios.get(`https://sharingvision-backend.herokuapp.com/user/`);
            if (res.status === 200) {
                setData(res.data.data);
                for (let index = 0; index < data.length; index++) {
                    arr.push(data[index].id)
                }
                setUserIds(arr)
                setDataFetched(true)
            }
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        getDatas()
    }, [])

    return (
        <div>
            <h1>Delete User</h1>
            <hr />

            <CButton color="primary" onClick={ handleModal }>
                Delete User
            </CButton>
            {isDeleted ? <h3>Successfully Deleted</h3> : <div></div>}

            <CModal
                show={createPop}
                onClose={() => setCreatePop(!createPop)}
                size="lg"
            >
                {console.log(userIds)}
                <CModalHeader closeButton>
                    <CModalTitle>Delete User</CModalTitle>
                </CModalHeader>
                <CModalBody>
                    {!dataFetched ? <CircularProgress/> : <div><TextField
                        label="User Id"
                        variant="outlined"
                        onChange={handleId}
                    />
                    <br />
                    {userIds.includes(parseInt(userId)) ? <div>
                        <p>Data ditemukan</p>
                        <span>Nama     : {name}</span><br />
                        <span>Username : {username}</span>
                    </div> : <p>Data tidak ditemukan</p>}</div>}
                    
                </CModalBody>
                <CModalFooter>
                    {userIds.includes(parseInt(userId)) ? (
                        <CButton onClick={() => deleteData()} color="primary">Delete It</CButton>
                    ) : (
                        <CButton color="primary" disabled>Can't delete</CButton>
                    )}
                    <CButton
                        color="secondary"
                        onClick={() => setCreatePop(false)}
                    >Cancel</CButton>
                </CModalFooter>
            </CModal>
        </div>
    )
}
