import React from 'react';

const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard.jsx'));
const Create = React.lazy(() => import('./views/create/User.jsx'));
const Delete = React.lazy(() => import('./views/delete/User.jsx'));

const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/user-management', name: 'User Management', component: Dashboard, exact: true},
  { path: '/user-management/create', name: 'Create User Management', component: Create },
  { path: '/user-management/delete', name: 'Delete User Management', component: Delete },
];

export default routes;
